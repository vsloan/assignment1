$(document).ready(function(){

  /*Smooth scroll added when user click on a anchor tag */
  $('a[href^="#"]').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

    /*Fade in Jumbotron Images */
    $('#picOne').fadeIn(1500);
    $('#picTwo').delay(1500).fadeIn(1500);
    $('#picThree').delay(3000).fadeIn(1500);

});
