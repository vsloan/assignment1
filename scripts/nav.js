$(document).ready(function() {
  /*Variable for top of browser*/
  var scrollTop = 0;


  /*CSS Functions grouped together */
     function largeNavCssChanges() {
       $("#navbar-brand").css("display", "block");
       $("#navbar").css("height", "200px");
       $(".navbar-links").css({"float":" none", "text-align" : "center"});
     }

     function smallNavCssChanges() {
       $("#navbar-brand").css("display", "none");
       $("#navbar").css("height", "70px");
       $(".navbar-links").css("float", "right");
     }

/*Function to show Large Nav -calls css function inside */
  function showLargeNav(){
    largeNavCssChanges();
    $(".navbar-smallLogo").addClass("navbar-smallLogo-hidden");
  }

/*Function to hide the largeNav and change to smaller Nav */
  function hideLargeNav() {
    smallNavCssChanges();
    $(".navbar-smallLogo").removeClass("navbar-smallLogo-hidden");
  }


/* Function called when user scrolls to determine
  whether to show small or large nav */
  $(window).scroll(function(){
    scrollTop = $(window).scrollTop();
    if (scrollTop >= 100) {
      hideLargeNav();
    } else if (scrollTop < 100) {
      showLargeNav();
    }
  });

});
